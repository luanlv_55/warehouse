name := """warehouse"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.10.3"

libraryDependencies ++= Seq (
  javaJdbc,
  javaEbean,
  cache,
  filters,
  //"org.postgresql" % "postgresql" % "9.4-1200-jdbc41"
  "org.postgresql" % "postgresql" % "9.3-1100-jdbc41"
)
